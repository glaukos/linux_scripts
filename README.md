# Useful Linux Scripts

1. Backup Script - backup_script.sh
    
    A simple backup script (incremental and full backup).
    
    Script Features
    - Incremental backup retention.
    - Currently supports GNU/Linux and FreeBSD.
    - Daily backups are retained for the past week.
    - Weekly backups from Sunday are retained for the past week.
	
2. Backup script on Tape - backup_tape.sh

    A simple backup script to take backup on tape device.

3. Monitor Disk Space - monitor_disk_space.sh
    
    A simple bash shell script to monitoring free disk space and send alert notifications with e-mail.
 
4. Backup DVD - backup_dvd.sh

    A simple backup script to take backup on DVD device.

5.	Reboot Notify - reboot.sh
	
	A simple script notify you when your server rebooted
    