#!/bin/bash
#
# Backup script - Backup on the DVD
# Version 1.1
# Create By Yiannis Panagou
# 
# Replace the following with actual locations on *your* system
#
# For debug please uncomment below line
#set -x
#
GROWISOFS=/usr/bin/growisofs
ECHO=/bin/echo
SLEEP=/bin/sleep
GREP=/bin/grep
MAIL=/bin/mail
today=`/bin/date +%A`
EJECT="/usr/bin/eject"
ADMINS=your@email.xxx
################################################
#
#  !!! PLEASE DO NOT CHANGE BELOW !!!
#
###############################################
# Backup begins below
$ECHO >> /tmp/backupoutput.tmp

sleep 5 

$ECHO "Beginning $today backup to DVD" > /tmp/backupoutput.tmp
$ECHO >> /tmp/backupoutput.tmp
$ECHO "Format and create a small iso set on DVD in case this is a new disc" >> /tmp/backupoutput.tmp
$ECHO "(the following may fail if disc already has data on it, no problem)" >> /tmp/backupoutput.tmp
$ECHO >> /tmp/backupoutput.tmp
$ECHO "Now creating actual $today backup" >> /tmp/backupoutput.tmp
$ECHO "(should not fail at this point)" >> /tmp/backupoutput.tmp
$ECHO >> /tmp/backupoutput.tmp
$GROWISOFS -dvd-compat -Z /dev/cdrom -D -J -R -T -l -graft-points -joliet-long -hide-joliet-trans-tbl\
 -iso-level 4 -overburn -V "$today Backup"\

 # Here add directory or files you want to take backup
 dir_1/=/dir_1\
 dir_2/=/dir_2\
 dir_3/=/dir_3\
 
# Modify graft points above as needed
mycode=$?
$ECHO >> /tmp/backupoutput.tmp
$ECHO "Return code from above is: $mycode" >> /tmp/backupoutput.tmp
$ECHO >> /tmp/backupoutput.tmp
$ECHO "End of $today backup" >> /tmp/backupoutput.tmp
$GREP -i -v sleeping /tmp/backupoutput.tmp > /tmp/backupoutput.tmp2
$GREP -i -v formatting /tmp/backupoutput.tmp2 > /tmp/backupoutput.txt
$MAIL -n -s "Output from DVD backup" $ADMINS < /tmp/backupoutput.txt

sleep 3

/usr/bin/eject

exit 0
