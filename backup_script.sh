#!/bin/bash
#
# Backup script - Full and Incremental
# Version 1.0
# Create By Yiannis Panagou
# For debug remove below hash
#set -x
#
#
SRC_PATH="/home /root/"
DST_PATH="/backup_export"
LIST="/tmp/backlist_$$.txt"
#
# -----------------------------------------------------------------------
#
# here I am setting a time stamp variable which I like to use for logging
TIMESTAMP=`date +%Y%m%d.%H%M`

# let's create a variable for the backup file name file
FNAME="Backup"

# let's create a variable for the log file, let's also name the log file with the filename and timestamp it
LOG="$FNAME-$TIMESTAMP.log"

set $(date)
if test "$1" = "Sun" ; then
        # Weekly a full backup of all data.
        #
        # Start the backup, create a log file which will record any messages run by this script
        #
        echo "*** Starting Full Backup -- $TIMESTAMP $SRC_PATH" >> ${LOG}
        tar cvfz "$DST_PATH/data/data_full_$6-$2-$3.tar.gz" $SRC_PATH >> ${LOG}
        # Delete Incremental Backup Files
        rm -f $DST_PATH/data/data_diff* >> ${LOG}
        # Delete old Backup files over 6 Days
        echo "*** Delete old full backup" >> ${LOG}
        find $DST_PATH/data/ -mtime +6 -exec rm {} \;
        echo "*** Ending backup of $SRC_PATH directories -- $TIMESTAMP" >> ${LOG}
        #
else
        # Incremental backup
        #
	# Start the backup, create a log file which will record any messages run by this script
        #
        echo "*** Starting Incremental Backup -- $TIMESTAMP $SRC_PATH" >> ${LOG}
        find $SRC_PATH -depth -type f \( -ctime -1 -o -mtime -1 \) -print > $LIST
        # start the backup, create a log file which will record any messages run by this script
        tar cvfzT  "$DST_PATH/data_diff_$6-$2-$3.tar.gz" "$LIST" >> ${LOG}
        rm -f "$LIST" >> ${LOG}
        echo "*** Ending backup of $SRC_PATH directories -- $TIMESTAMP" >> ${LOG}
fi

# End the backup, append to log file created by this script
echo "*** Ending total backup -- $TIMESTAMP ***" >> ${LOG}
