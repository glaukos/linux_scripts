#!/bin/sh
# Backup script in the Tape Device
# Version 1.0
# Created by Yiannis Panagou
#
#Set Tape Device here
TAPE=/dev/rmt/0
#
#Set Source Path where take backup
DIR=/dir /dir1 /dir2
#
#Set Log Directory
LOGDIR=/var/log/backup
#
#Set export Logfile Name
LOGFILE=$LOGDIR/backup_`date '+%Y%m%d'`
#
# 
#=====================================
# Please do not remove anything from
# below lines!!!!!
#=====================================
#
echo "`date '+%Y%m%d-%H%M'` Starting backup to tape ..." >> $LOGFILE 2>&1

find $LOGDIR -name backup_\* -mtime +7 -exec rm {} \;

mt -f $TAPE rewind > /dev/null 2>&1
tape_status=$?

if [ $tape_status -ne 0 ]; then
  echo "`date '+%Y%m%d-%H%M'` Tape drive problem ..." >> $LOGFILE 2>&1
  exit 1
fi

mt -f $TAPE rewind > /dev/null 2>&1
echo "`date '+%Y%m%d-%H%M'` Starting tar ..." >> $LOGFILE 2>&1
tar cvf $TAPE $DIR >> $LOGFILE 2>&1
echo "`date '+%Y%m%d-%H%M'` Finished tar and ejecting tape ..." >> $LOGFILE 2>&1

mt -f $TAPE offline