#!/bin/sh
# Monitor Disk Space
# Support CentOS, Debian, Ubuntu
# Created by Yiannis Panagou
# Version 1.0
#
#-----------------------------------------
# Put below line your email
#-----------------------------------------
ADMIN=email@address
#-----------------------------------------
STATUS_C=90 # Set here Critical Alert level
STATUS_W=99 # Set here Warning Alert Level
#-----------------------------------------
#
# Please uncomment below line for debug
#set -x
#-----------------------------------------
#
# Do not change anything from below lines
#
#=========================================
#
/bin/df -HP | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5 " " $1 }' | while read output;
do
echo $output
usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1 )
partition=$(echo $output | awk '{ print $2 }' )

if [ $usep -ge $STATUS_C ]; then
#
echo "Running disk space is FULL \"$partition ($usep%)\" on $(hostname) as on $(date +%d-%m-%Y)" |
mail -s "!!! CRITICAL: Disk space is FULL !!! $usep%" $ADMIN
#
else
if [ $usep -ge $STATUS_W ]; then
#
echo "Running out of space \"$partition ($usep%)\" on $(hostname) as on $(date +%d-%m-%Y)" |
mail -s " WARNING: Almost out of disk space $usep%" $ADMIN
#
fi
fi
done