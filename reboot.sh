#!/bin/sh
# Script notify you when your server rebooted
# Created by Yiannis Panagou
# Version 1.0
DATE="`date '+%x %H:%M:%S'`"
SRVNM=`uname -n`
MAILADD="your@email.xxx"
mail $MAILADD -s " Boot of $SRVNM "<<EOF
$DATE
$SRVNM has booted up.
If this is news to you, please investigate.
EOF
